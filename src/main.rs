use std::fmt::Display;

#[derive(Debug, Clone, PartialEq, Eq)]
struct CharRange {
    start: char,
    end: char,
}
impl CharRange {
    fn contains(&self, c: char) -> bool {
        return c >= self.start && c <= self.end;
    }
    fn from_char(c: char) -> Self {
        return Self { start: c, end: c };
    }
    fn from_start_end(start: char, end: char) -> Self {
        return Self { start, end };
    }
}
impl Display for CharRange {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.start == self.end {
            write!(f, "{}", self.start)
        } else {
            write!(f, "{}..={}", self.start, self.end)
        }
    }
}

#[derive(Debug, Clone)]
struct Rule {
    conditions: Vec<CharRange>,
    target: usize,
}
impl Rule {
    pub fn matches(&self, c: char) -> bool {
        self.conditions.iter().any(|cond| cond.contains(c))
    }
}
#[derive(Debug, Clone)]
struct State {
    rules: Vec<Rule>,
    can_be_end: bool,
}
impl State {
    pub fn step(&self, c: char) -> Option<usize> {
        if let Some(rule) = self.rules.iter().filter(|rule| rule.matches(c)).next() {
            Some(rule.target)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone)]
struct ParseRules(Vec<State>);
impl ParseRules {
    pub fn empty() -> Self {
        return Self(Vec::new());
    }
    pub fn single_character(c: char) -> Self {
        Self::single_character_from_range(c, c)
    }
    pub fn single_character_from_range(start: char, end: char) -> Self {
        let mut rules = Self::empty();
        rules.0.push(State {
            rules: vec![Rule {
                conditions: vec![CharRange::from_start_end(start, end)],
                target: 1,
            }],
            can_be_end: false,
        });
        rules.0.push(State {
            // post
            rules: vec![],
            can_be_end: true,
        });
        rules
    }
    pub fn match_string(s: &str) -> Option<Self> {
        let mut rules = Self::single_character(s.chars().nth(0)?);
        for c in s.chars().skip(1) {
            rules.chain(&Self::single_character(c));
        }
        Some(rules)
    }

    pub fn chain(&mut self, other: &ParseRules) {
        if other.0.len() == 0 {
            // No other states, do nothing
            return;
        }
        let other_initial = &other.0[0];
        let state_offset = self.0.len() - 1;
        // merge other's initial state with the current final ones
        for (i, state) in self.0.iter_mut().enumerate().filter(|(_, s)| s.can_be_end) {
            for mut transition in other_initial.rules.iter().cloned() {
                if transition.target == 0 {
                    // Back to initial, target self
                    transition.target = i;
                } else {
                    transition.target += state_offset;
                }
                state.rules.push(transition)
            }
            state.can_be_end = other_initial.can_be_end;
        }
        // append non-initial states from other to this rules
        self.append_other_states(other);
    }
    pub fn zero_or_many(&mut self) {
        // zero or many is the same as one or many but optional
        self.one_or_many();
        self.optional();
    }
    pub fn one_or_many(&mut self) {
        // loop by allowing all transitions from initial state on the final states
        let (initial, following) = self.0.as_mut_slice().split_at_mut(1);
        let initial = &mut initial[0];
        for state in following.iter_mut().filter(|s| s.can_be_end) {
            for transition in initial.rules.iter().cloned() {
                state.rules.push(transition);
            }
        }
    }
    pub fn optional(&mut self) {
        // initial state can be final -> this is entirely optional
        self.0[0].can_be_end = true;
    }

    fn append_other_states(&mut self, other: &ParseRules) -> usize {
        let state_offset = self.0.len() - 1;
        for state in other.0.iter().skip(1) {
            let mut new_state = state.clone();
            for rule in new_state.rules.iter_mut() {
                if rule.target > 0 {
                    rule.target += state_offset;
                } else {
                    panic!(
                        "Non-initial states cannot have a transition back to the initial state!"
                    );
                }
            }
            self.0.push(new_state)
        }
        state_offset
    }

    /// Adds a branching option at the current position.
    /// This means that at the resulting initial state, all strings accepted by self and other are
    /// valid.
    pub fn branch(&mut self, other: &ParseRules) {
        let other_offset = self.append_other_states(other);

        let mut stack = Vec::new();
        let mut checked = Vec::new();
        stack.push((0, 0));
        while let Some((self_state, other_state)) = stack.pop() {
            checked.push((self_state, other_state));
            for transition in other.0[other_state].rules.iter() {
                if let Some(trans_self) = self.0[self_state]
                    .rules
                    .iter()
                    .find(|t| t.conditions == transition.conditions)
                {
                    let new_pair = (trans_self.target, transition.target);
                    if !stack.contains(&new_pair) && !checked.contains(&new_pair) {
                        stack.push(new_pair);
                    }
                } else {
                    // No matching transition found in the existing graph
                    let mut trans_clone = transition.clone();
                    trans_clone.target += other_offset;
                    self.0[self_state].rules.push(trans_clone);
                }
            }
        }

        // Some unused states stay here, prune them?
    }
}

impl Display for ParseRules {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (idx, state) in self.0.iter().enumerate() {
            write!(
                f,
                "State {} {}: \n",
                idx,
                if state.can_be_end { "(End)" } else { "" }
            )?;
            for transisition in state.rules.iter() {
                write!(f, "{:indent$}->{}:", "", transisition.target, indent = 4)?;
                for cond in transisition.conditions.iter() {
                    write!(f, " {}", cond)?;
                }
                write!(f, "\n")?;
            }
        }
        Ok(())
    }
}

struct RulesRunner<'a> {
    rules: &'a ParseRules,
    state: usize,
}
enum RunResult {
    Accept,
    Reject,
    Advance,
}
impl<'a> RulesRunner<'a> {
    pub fn new(rules: &'a ParseRules) -> Self {
        Self { rules, state: 0 }
    }
    pub fn step(&mut self, c: char) -> RunResult {
        let st = &self.rules.0[self.state];
        if let Some(new_state) = st.step(c) {
            self.state = new_state;
            if self.rules.0[new_state].can_be_end {
                RunResult::Accept
            } else {
                RunResult::Advance
            }
        } else {
            RunResult::Reject
        }
    }
}

fn run_rules<'a>(input: &'a str, rules: &ParseRules) -> &'a str {
    let mut runner = RulesRunner::new(rules);
    let mut accepted = 0;
    for (i, c) in input.chars().enumerate() {
        match runner.step(c) {
            RunResult::Accept => {
                accepted = i + 1;
            }
            RunResult::Advance => {}
            RunResult::Reject => return &input[..accepted],
        }
    }
    return &input[..accepted];
}

fn main() {
    let input = "123_456_78 abc";
    let num_rules = ParseRules::single_character_from_range('0', '9');
    let us_rules = ParseRules::single_character('_');

    let mut numbers_string = num_rules.clone();
    numbers_string.one_or_many();

    let mut underscore_num = us_rules.clone();
    underscore_num.chain(&numbers_string);
    underscore_num.zero_or_many();
    let mut seperated_number = numbers_string.clone();
    seperated_number.chain(&underscore_num);

    let match_foo = ParseRules::match_string("foo").unwrap();
    let match_bar = ParseRules::match_string("bar").unwrap();
    let match_baz = ParseRules::match_string("baz").unwrap();
    let mut match_combined = match_foo.clone();
    match_combined.branch(&match_bar);
    match_combined.branch(&match_baz);

    println!("parsing result: {}", run_rules(input, &num_rules));
    println!(
        "parsing underscore on number: {}",
        run_rules(input, &us_rules)
    );
    println!("parsing underscore: {}", run_rules("_abc", &us_rules));
    println!(
        "parsing seperated number: {}",
        run_rules(input, &seperated_number)
    );

    println!("parsing bar: {}", run_rules("barhello", &match_combined));
    println!("foobazbar graph: \n{}", match_combined);
}
